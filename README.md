## MarkLogic Spark Examples ##

Example application that demonstrate how to use MarkLogic with Apache Spark

### Prerequisites ###

1. You have Hadoop 2.7.1 or above and Spark 1.3.1 or above  setup.
2. You have MarkLogic 9.0.5 or above installed and running.

### Build ###
The jar files are included. If you want to rebuild, you can run the following commands

1. Go to folder Connector and run
	mvn package
2. Go to folder Spark and run the following two commands
Copy the newly compiled ml-spark-java-connector-0.0.1-SNAPSHOT.jar from Connector folder to a subfolder /lib in this sourceDirectory
Run the following commands
mvn install:install-file -Dfile=./lib/ml-spark-java-connector-0.0.1-SNAPSHOT.jar -DgroupId=com.marklogic.spark -DartifactId=ml-spark-java-connector -Dversion=1.0 -Dpackaging=jar

mvn package


### Setup ###

#####Setup Database#####

1. Unzip the sample data available in sample directory.
2. Use mlcp to import data into a MarkLogic database.

### Usage ###
spark-submit --class "spark.LinearRegressionThreaded" --master local[4] \
  <your directory>/spark-0.0.1-SNAPSHOT-jar-with-dependencies.jar \
  <your directory>/template.properties


### Next Steps ###
We will add more connection mechanism to this demo
